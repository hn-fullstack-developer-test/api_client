import { Component, OnInit } from "@angular/core";
import { ChangeDetectorRef } from '@angular/core';
import { DataApiService } from "./services/data-api.service";
import es from "@angular/common/locales/es";
import { registerLocaleData } from '@angular/common';

import { MatTableDataSource } from '@angular/material';
import { Post } from '../app/models/post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = "HN Feed"
  public displayedColumns = ['title', 'created_at', 'delete'];
  public dataSource = new MatTableDataSource<Post>();
  spiner: boolean = false;

  constructor(
    private dataApi: DataApiService,
    private cdref: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    registerLocaleData(es);
    this.getData();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  getData() {
    this.dataApi.getPostsAsync().then((data: any) => {
      console.log("data", data);
      if(data !== undefined) {
        data.forEach(e => {
          e["timestamp"] = this.toTimestamp(e.created_at);
        });
        data.sort((a,b) => (a.timestamp > b.timestamp) ? -1 : ((b.timestamp > a.timestamp) ? 1 : 0)); 
        this.dataSource.data = data as Post[];
      }

    }).then(() => this.spiner = false);
  }

  navigateTo(row: Post) {
    window.open(row.url, "_blank");
  }

  delete(row: Post) {
    this.spiner = true;
    this.dataApi.delPostsAsync(row._id).then(() => {
      this.getData();
      
    });
  }

  toTimestamp(strDate){
    var datum = Date.parse(strDate);
    return datum/1000;
 }
  
}
