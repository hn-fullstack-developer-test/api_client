export interface Post{
    _id: string;
    created_at: Date;
    timestamp?: string;
    title: string;
    author: string;
    url: string
}