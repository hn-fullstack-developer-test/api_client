import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material';

import { MatTableModule } from '@angular/material';
import { MatJumbotronModule } from '@angular-material-extensions/jumbotron';
import { DateFormaterPipe } from './pipes/date-formater.pipe';

@NgModule({
  declarations: [
    AppComponent,
    DateFormaterPipe
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatTableModule,
    MatJumbotronModule
  ],
  exports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatTableModule,
    MatJumbotronModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
