"use stricts";

let chai = require("chai");
let chaiHttp = require("chai-http");
const expect = require("chai").expect;

chai.use(chaiHttp);

const request = require('supertest');
const app = require('../app');

let fechaActual = new Date();
let insertedId = "";

// ! ___________________________________________

describe('App', function(done) {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });

  it('has /posts page', function(done) {
    request(app)
      .get('/posts')
      .expect(200, done);
  });

}); 

describe('Get all posts:', () => {
  it("should get all posts", done => {
    chai
      .request(app)
      .get('/posts')
      .end(function(err, res) {
        console.log('Posts Listed: ', res.body.length);
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe('Insert a post: ', () => {
  it("should insert a post", done => {
    chai
      .request(app)
      .post('/post')
      .send({
        created_at: fechaActual,
        title: "Post de Prueba",
        autor: "Alejandro J Araujo",
        url: "http://hn.algolia.com/api/v1/search_by_date?query=nodejs"
      })
      .end(function(err, res) {
        console.log(res.body);
        insertedId = res.body._id;
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe('Delete the post inserted in the last step:', () => {
  it('should delete the last post inserted', done => {
    console.log('Post id: ', insertedId);
    const toDelete = '/post/' + insertedId;
    chai
      .request(app)
      .del(toDelete)
      .end(function(err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });
});
