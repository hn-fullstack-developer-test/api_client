const fetch = require('node-fetch');
const cron = require("node-cron");

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const user = 'testhn'
const password = 'pXpKKC1qPNlDrqZG'
const url = `mongodb://${user}:${password}@` +
    'cluster0-shard-00-00-gvjef.mongodb.net:27017,' +
    'cluster0-shard-00-01-gvjef.mongodb.net:27017,' +
    'cluster0-shard-00-02-gvjef.mongodb.net:27017/hn?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin';

// Database Name
const dbName = 'hn';

// Create a new MongoClient
const client = new MongoClient(url, { 'useUnifiedTopology': true, 'useNewUrlParser': true });

// Use connect method to connect to the Server
client.connect(function (err) {
    assert.equal(null, err);
    console.log("Connected successfully to Algolia server");

    const db = client.db(dbName);

    console.log(new Date())
    cron.schedule('0 * * * *', () => {
        console.log(new Date())
        getAlgoliaData(db, () => {
            client.close();
        });
    });
});


async function getAlgoliaData(db) {
    try {
        let data = await fetch('http://hn.algolia.com/api/v1/search_by_date?query=nodejs');
        let main = await data.json();

        var arr = [];
        for (var id in main.hits) {
            var title = "";
            if (main.hits[id]["story_title"] === null && main.hits[id]["title"] === null) {
                continue;
            } else if (main.hits[id]["story_title"] !== null) {
                title = main.hits[id]["story_title"]
            } else if (main.hits[id]["story_title"] === null) {
                title = main.hits[id]["title"]
            }

            var url = "";
            if (main.hits[id]["story_url"] === null && main.hits[id]["url"] === null) {
                continue;
            } else if (main.hits[id]["story_url"] !== null) {
                url = main.hits[id]["story_url"]
            } else if (main.hits[id]["story_url"] === null) {
                url = main.hits[id]["url"]
            }

            arr.push({
                "created_at": main.hits[id]["created_at"],
                "title": title,
                "author": main.hits[id]["author"],
                "url": url
            });
        }

        const collection = db.collection('posts');
        collection.createIndex({ groupNumber: 1, created_at: 1, autor: 1 }, { unique: true })
        // Insert all documents
        await collection.insertMany(arr);
    }
    catch (err) { }
}


