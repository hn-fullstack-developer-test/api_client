# Server component + Client Component: Node.js + Express + MongoDB(Cloud) + Angular 8 + Angular Material

This project contains the API/Fetch funcionalities and the Angular client of the HN (Fullstack Developer Test) project.

## CI/CD
It is built with CI/CD functionalities through the .gitlad-ci.yml file located at the root of the project.


## Run Local API & Client via Docker

A docker-compose.yml file that allows a compose, multi-stage build was also added, to run the functionality you must have Docker & docker-compose  installed on your system, clone the project, cd to it and run the following command to create and run the entire project:

`docker-compose up -d`


To check its operation visit the *http://localhost* url in the browser.


## Turn off

To turn off project containers:

`docker-compose down`


